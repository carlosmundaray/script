// ==UserScript==
// @name         index
// @namespace    http://tampermonkey.net/
// @version      1.2
// @description  try to take over the world!
// @author       Itachi#1117
// @updateURL    https://bitbucket.org/carlosmundaray/script/raw/4e73ae4d30575e7c08fe375d739ae7ed26a07485/index.js
// @downloadURL  https://bitbucket.org/carlosmundaray/script/raw/4e73ae4d30575e7c08fe375d739ae7ed26a07485/index.js
// @match        https://view.appen.io/assignments/*
// @match        http://localhost/figure/*
// @grant        GM_xmlhttpRequest
// @require      https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js
// @connect      account.appen.com
// @connect      tasks.figure-eight.work
// @connect      aka-tsukis.com
// @run-at       document-end
// ==/UserScript==

(function() {
//base de url de la api
var base_url="http://aka-tsukis.com/api";

//datos autentication
var worker_id=Cookies.get('worker_id')||'';
var jwtoken=Cookies.get('jwtoken')||'';

var titleDivapp = document.createElement("div");
var titleapp = document.querySelector(".job-title").innerText;
titleDivapp.innerHTML = titleapp;
var jobTitle = titleDivapp.innerText;


//Amount && ingredient
if (jobTitle.toLowerCase().includes("amount and unit of measure annotation") || jobTitle.toLowerCase().includes("ingredient entity annotation")) {
  
  //tiempo en milisegundos !!!
  var time_submit_min=10000;
  var time_submit_max=11000;

  var guide = [];
  var task_names = [];
  var continuar=false;    
      
  var jsawesome = document.querySelectorAll(".jsawesome");
      jsawesome.forEach(wrapper => {
          var titleDiv = wrapper.querySelector("div");
          var titulo = wrapper.querySelector(".html-element-wrapper>table>tbody>tr>td").textContent;
          var pares_content = wrapper.querySelectorAll(".radios.cml_field");
          guide.push(titulo);
      });

      GM_xmlhttpRequest({
          method: "POST",
          url: base_url+"/amount_ingredient",
          data:JSON.stringify ( {"worker_id":worker_id, "name":guide} ),
          headers:    {
              Authorization: "Bearer " + jwtoken,
              "Content-Type": "application/json",
              "Accept": "application/json"
          },
          onload: function(response) {
              console.log(response);
              var json=JSON.parse(response.responseText)||0;
              if (!JSON.parse(response.responseText).errors && response.status==200) {
                  Object.entries(json).forEach(([key, value]) => {
                      task_names.push(value);
                  });
                  set(task_names);
              }else{
                  var errors = document.querySelectorAll(".jsawesome");
                  errors.forEach(error => {
                      var titleDiv = error.querySelector("div");
                          titleDiv.style.backgroundColor = "red";
                          titleDiv.scrollIntoView({ block: "center" });
                  });
              }
          },
          onerror:function(resp){
            console.log("Error");
            console.log(resp.status+' '+resp.statusText);
          },
          ontimeout:function(resp){
            console.log('Timeout');
          }
      });


      function set(task_names){

        const filterItems = query => {
              return Object.values(Object.values(task_names).filter((el) =>el.name.toLowerCase().indexOf(query.name.toLowerCase()) > -1));
        }


        var jsawesome = document.querySelectorAll(".jsawesome");

            jsawesome.forEach(wrapper => {
                var titleDiv = wrapper.querySelector("div");
                var titulo = wrapper.querySelector(".html-element-wrapper>table>tbody>tr>td").textContent.trim().replace(/(\r\n\t|\n|\r\t)/gm,"");
                var pares_content = wrapper.querySelectorAll(".radios.cml_field");
                var fill = filterItems({'name':titulo});

                if(fill[0]?.name.toLowerCase()==titulo.toLowerCase()){
                   titleDiv.style.backgroundColor = "Aqua";
                   titleDiv.scrollIntoView({ block: "center" });

                    for(var preg of pares_content.entries()) {
                        let radios = preg[1].querySelectorAll(".radios.cml_field input");
                        let indice=fill[0].respuesta.charAt(preg[0])-1;
                        if(indice!==-1){
                            radios[indice].checked=1;
                            radios[indice].click();
                        }
                    }

                }else{

                   for(var pregg of pares_content.entries()) {
                        let radios = pregg[1].querySelectorAll(".radios.cml_field input");
                        let st="1";
                        let indice=st.charAt(pregg[0])-1;
                        if(indice!==-1){
                            radios[indice].checked=1;
                            radios[indice].click();
                        }
                    }

                }


            });

     }

    function time_submit(min, max){
            return Math.floor(Math.random() * (max - min + 1) + min);
    }

    function ActivarReloj(){
        //Obteniendo datos del tiempo
        var laHora = new Date();
        var horario = laHora.getHours();
        var minutero = laHora.getMinutes();
        var segundero = laHora.getSeconds();

        if(minutero<10){
            minutero = "0" + minutero;
        }
        if(segundero<10){
            segundero = "0" + segundero;
        }

        var tiempo = document.querySelectorAll("#countdown_timer .countdown_row").length;

        if(tiempo != 0){
            let submit=time_submit(time_submit_min,time_submit_max);
            //console.log("se enviara en "+submit);
            document.title = "("+submit+") OPEN";
            setTimeout(function () {
                document.title = "CERRANDO";
                document.querySelector(".btn-cf-blue").click();
            }, submit);
        }else{
            document.title = "TIEMPO ESPERA: "+horario+":"+minutero+":"+segundero;
            //Actualizando la hora cada 1 segundo
            setTimeout(ActivarReloj,1000);
        }

    }
    ActivarReloj();

}//end-amount


//easy as it gets.
if (jobTitle.toLowerCase().includes("easy as it gets.")) {

//tiempo en milisegundos !!!
var time_submit_min=30000;
var time_submit_max=31000;
var guide = [];
var task_names = [];
var continuar=false;

  var jsawesome = document.querySelectorAll(".jsawesome");
      jsawesome.forEach(wrapper => {
          var titleDiv = wrapper.querySelector("div");
          var url = wrapper.querySelector(".html-element-wrapper > iframe").src;
          guide.push(url);
      });

      GM_xmlhttpRequest({
          method: "POST",
          url: base_url+"/easyasitgets",
          data:JSON.stringify ( {"worker_id":worker_id, "name":guide} ),
          headers:    {
              Authorization: "Bearer " + jwtoken,
              "Content-Type": "application/json",
              "Accept": "application/json"
          },
          onload: function(response) {
              var json=JSON.parse(response.responseText)||0;
              if (!JSON.parse(response.responseText).errors && response.status==200) {
                  Object.entries(json).forEach(([key, value]) => {
                      task_names.push(value);
                  });
                  set(task_names);
              }else{
                  var errors = document.querySelectorAll(".jsawesome");
                  errors.forEach(error => {
                      var titleDiv = error.querySelector("div");
                          titleDiv.style.backgroundColor = "red";
                          titleDiv.scrollIntoView({ block: "center" });
                  });
              }
          },
          onerror:function(resp){
            console.log("Error");
            console.log(resp.status+' '+resp.statusText);
          },
          ontimeout:function(resp){
            console.log('Timeout');
          }
      });


      function set(task_names){

        const filterItems = query => {
              return Object.values(Object.values(task_names).filter((el) =>el.name.toLowerCase().indexOf(query.name.toLowerCase()) > -1));
        }

        var jsawesome = document.querySelectorAll(".jsawesome");

            jsawesome.forEach(wrapper => {
                var titleDiv = wrapper.querySelector("div");
                var url = wrapper.querySelector(".html-element-wrapper > iframe").src;
                var fill = filterItems({'name':url});

                if(fill[0]?.name.toLowerCase()==url.toLowerCase()){
                      titleDiv.style.backgroundColor = "Aqua";
                      titleDiv.scrollIntoView({ block: "center" });
                      var id= wrapper.id;
                      let indice=fill[0].respuesta-1;
                      var primero3 = document.getElementsByName(id+"[label][]");
                      primero3[indice].click();

                }else{

                      var idd2= wrapper.id;
                      var primero2 = document.getElementsByName(idd2+"[label][]");
                      primero2[4].click();

                }
            });
     }

    function time_submit(min, max){
            return Math.floor(Math.random() * (max - min + 1) + min);
    }

    function ActivarReloj(){
        //Obteniendo datos del tiempo
        var laHora = new Date();
        var horario = laHora.getHours();
        var minutero = laHora.getMinutes();
        var segundero = laHora.getSeconds();

        if(minutero<10){
            minutero = "0" + minutero;
        }
        if(segundero<10){
            segundero = "0" + segundero;
        }

        var tiempo = document.querySelectorAll("#countdown_timer .countdown_row").length;

        if(tiempo != 0){
            let submit=time_submit(time_submit_min,time_submit_max);
            //console.log("se enviara en "+submit);
            document.title = "("+submit+") OPEN";
            setTimeout(function () {
                document.title = "CERRANDO";
                document.querySelector(".btn-cf-blue").click();
            }, submit);
        }else{
            document.title = "TIEMPO ESPERA: "+horario+":"+minutero+":"+segundero;
            //Actualizando la hora cada 1 segundo
            setTimeout(ActivarReloj,1000);
        }

    }
    ActivarReloj();

}
//easy as it gets.

//relevant chinese social media
if (jobTitle.toLowerCase().includes("relevant chinese social media")) {

//tiempo en milisegundos !!!
var time_submit_min=10000;
var time_submit_max=11000;

var guide = [];
var task_names = [];
var continuar=false;

    var jsawesome = document.querySelectorAll(".jsawesome");
    var max_tq = jsawesome.length;
    var tq_found = 0;
      jsawesome.forEach(wrapper => {
          var titleDiv = wrapper.querySelector("div");
          var titulo = wrapper.querySelectorAll(".html-element-wrapper span")[0].textContent.trim().replace(/(\r\n\t|\n|\r\t)/gm,"");
          guide.push(titulo);
      });

      GM_xmlhttpRequest({
          method: "POST",
          url: base_url+"/chineses",
          data:JSON.stringify ( {"worker_id":worker_id, "name":guide} ),
          headers:    {
              Authorization: "Bearer " + jwtoken,
              "Content-Type": "application/json",
              "Accept": "application/json"
          },
          onload: function(response) {
                              console.log(response);
              var json=JSON.parse(response.responseText)||0;
              if (!JSON.parse(response.responseText).errors && response.status==200) {
                  Object.entries(json).forEach(([key, value]) => {
                      task_names.push(value);
                  });
                  set(task_names);
              }else{
                  var errors = document.querySelectorAll(".jsawesome");
                  errors.forEach(error => {
                      var titleDiv = error.querySelector("div");
                          titleDiv.style.backgroundColor = "red";
                          titleDiv.scrollIntoView({ block: "center" });
                  });
              }
          },
          onerror:function(resp){
            console.log("Error");
            console.log(resp.status+' '+resp.statusText);
          },
          ontimeout:function(resp){
            console.log('Timeout');
          }
      });


      function set(task_names){

        const filterItems = query => {
              return Object.values(Object.values(task_names).filter((el) =>el.name.toLowerCase().indexOf(query.name.toLowerCase()) > -1));
        }

        var jsawesome = document.querySelectorAll(".jsawesome");
            var no_existe=0;
            jsawesome.forEach(wrapper => {

               var titleDiv = wrapper.querySelector("div");
               var titulo = wrapper.querySelectorAll(".html-element-wrapper span")[0].textContent.trim().replace(/(\r\n\t|\n|\r\t)/gm,"");
               var pares_content = wrapper.querySelectorAll(".radios.cml_field");
               var fill = filterItems({'name':titulo});

                if(fill[0]?.name.toLowerCase()==titulo.toLowerCase()){

                   titleDiv.style.backgroundColor = "Aqua";
                   titleDiv.scrollIntoView({ block: "center" });

                    for(var preg of pares_content.entries()) {
                        let radios = preg[1].querySelectorAll(".radios.cml_field input");
                        let indice=fill[0].respuesta.charAt(preg[0])-1;
                        if(indice!==-1){
                            radios[indice].checked=1;
                            radios[indice].click();
                        }
                    }
                    tq_found++;

                }else{
                    var cont=1;
                    var iddd= wrapper.id;
                    var primero2 = document.getElementsByName(iddd+"[relevant]");
                    if(no_existe===0){cont=0;}
                    primero2[cont].checked=1;
                    primero2[cont].click();
                    no_existe++;
                }

            });
     }

     function comprobar_tq(){
         if(mode.includes("quiz")){
             if(tq_found<max_tq){
                 document.title="("+(max_tq-tq_found)+")"+" Manual";
                 return false;
             }
         }
         return true;
     }


    function time_submit(min, max){
            return Math.floor(Math.random() * (max - min + 1) + min);
    }

    function ActivarReloj(){
        //Obteniendo datos del tiempo
        var laHora = new Date();
        var horario = laHora.getHours();
        var minutero = laHora.getMinutes();
        var segundero = laHora.getSeconds();

        if(minutero<10){
            minutero = "0" + minutero;
        }
        if(segundero<10){
            segundero = "0" + segundero;
        }

        var tiempo = document.querySelectorAll("#countdown_timer .countdown_row").length;

        if(tiempo != 0){
            let submit=time_submit(time_submit_min,time_submit_max);
            //console.log("se enviara en "+submit);
            document.title = "("+submit+") OPEN";
            setTimeout(function () {
                if(comprobar_tq()){
                    document.title = "CERRANDO";
                    document.querySelector(".btn-cf-blue").click();
                }
            }, submit);
        }else{
            document.title = "TIEMPO ESPERA: "+horario+":"+minutero+":"+segundero;
            //Actualizando la hora cada 1 segundo
            setTimeout(ActivarReloj,1000);
        }

    }
    ActivarReloj();

}
//relevant chinese social media


//skill paraphrases evaluation
if (jobTitle.toLowerCase().includes("skill paraphrases evaluation")) {

//tiempo en milisegundos !!!
var time_submit_min=10000;
var time_submit_max=11000;
var guide = [];
var task_names = [];
var continuar=false;

  var jsawesome = document.querySelectorAll(".jsawesome");
      jsawesome.forEach(wrapper => {
          var titleDiv = wrapper.querySelector("div");
          var name1 = wrapper.querySelectorAll(".html-element-wrapper")[2].querySelector("span").textContent.trim().replace(/(\r\n\t|\n|\r\t)/gm,"");
          var name2 = wrapper.querySelectorAll(".html-element-wrapper")[3].querySelector("span").textContent.trim().replace(/(\r\n\t|\n|\r\t)/gm,"");
          guide.push({"name":name1,"name2":name2});
      });

      GM_xmlhttpRequest({
          method: "POST",
          url: base_url+"/skillparaphrases",
          data:JSON.stringify ( {"worker_id":worker_id, "name":guide} ),
          headers:    {
              Authorization: "Bearer " + jwtoken,
              "Content-Type": "application/json",
              "Accept": "application/json"
          },
          onload: function(response) {
              console.log(response);
              var json=JSON.parse(response.responseText)||0;
              if (!JSON.parse(response.responseText).errors && response.status==200) {
                  Object.entries(json).forEach(([key, value]) => {
                      task_names.push(value);
                  });
                  set(task_names);
              }else{
                  var errors = document.querySelectorAll(".jsawesome");
                  errors.forEach(error => {
                      var titleDiv = error.querySelector("div");
                          titleDiv.style.backgroundColor = "red";
                          titleDiv.scrollIntoView({ block: "center" });
                  });
              }
          },
          onerror:function(resp){
            console.log("Error");
            console.log(resp.status+' '+resp.statusText);
          },
          ontimeout:function(resp){
            console.log('Timeout');
          }
      });


      function set(task_names){

        const filterItems = query => {
              return Object.values(Object.values(task_names).filter((el) =>el.name.toLowerCase().indexOf(query.name.toLowerCase()) > -1 && el.name2.toLowerCase().indexOf(query.name2.toLowerCase()) > -1));
        }

        var jsawesome = document.querySelectorAll(".jsawesome");

            jsawesome.forEach(wrapper => {
                var titleDiv = wrapper.querySelector("div");
                var name1 = wrapper.querySelectorAll(".html-element-wrapper")[2].querySelector("span").textContent.trim().replace(/(\r\n\t|\n|\r\t)/gm,"");
                var name2 = wrapper.querySelectorAll(".html-element-wrapper")[3].querySelector("span").textContent.trim().replace(/(\r\n\t|\n|\r\t)/gm,"");
                var pares_content = wrapper.querySelectorAll(".radios.cml_field");
                var fill = filterItems({'name':name1,'name2':name2});

                if(fill[0]?.name.toLowerCase()==name1.toLowerCase() && fill[0]?.name2.toLowerCase()==name2.toLowerCase()){
                   titleDiv.style.backgroundColor = "Aqua";
                   titleDiv.scrollIntoView({ block: "center" });

                   for(var preg of pares_content.entries()) {
                        let radios = preg[1].querySelectorAll(".radios.cml_field input");
                        let indice=fill[0].respuesta.charAt(preg[0])-1;
                        if(indice!==-1){
                            radios[indice].checked=1;
                            radios[indice].click();
                        }
                    }

                }else{

                   for(var pregg of pares_content.entries()) {
                        let radios = pregg[1].querySelectorAll(".radios.cml_field input");
                        let st="31";
                        let indice=st.charAt(pregg[0])-1;
                        if(indice!==-1){
                            radios[indice].checked=1;
                            radios[indice].click();
                        }
                    }

                }
            });

     }

    function time_submit(min, max){
            return Math.floor(Math.random() * (max - min + 1) + min);
    }

    function ActivarReloj(){
        //Obteniendo datos del tiempo
        var laHora = new Date();
        var horario = laHora.getHours();
        var minutero = laHora.getMinutes();
        var segundero = laHora.getSeconds();

        if(minutero<10){
            minutero = "0" + minutero;
        }
        if(segundero<10){
            segundero = "0" + segundero;
        }

        var tiempo = document.querySelectorAll("#countdown_timer .countdown_row").length;

        if(tiempo != 0){
            let submit=time_submit(time_submit_min,time_submit_max);
            //console.log("se enviara en "+submit);
            document.title = "("+submit+") OPEN";
            setTimeout(function () {
                document.title = "CERRANDO";
                document.querySelector(".btn-cf-blue").click();
            }, submit);
        }else{
            document.title = "TIEMPO ESPERA: "+horario+":"+minutero+":"+segundero;
            //Actualizando la hora cada 1 segundo
            setTimeout(ActivarReloj,1000);
        }

    }
    ActivarReloj();

}
//skill paraphrases evaluation

//uk_search-science
if (jobTitle.toLowerCase().includes("uk_search-science")) {
//tiempo en milisegundos !!!
var time_submit_min=10000;
var time_submit_max=11000;
var guide = [];
var task_names = [];
var continuar=false;

  var jsawesome = document.querySelectorAll(".jsawesome");
      jsawesome.forEach(wrapper => {
          var titleDiv = wrapper.querySelector("div");
          var query = wrapper.querySelectorAll(".row-fluid .special")[0].textContent.trim().replace(/(\r\n\t|\n|\r\t)/gm,"");
          var result = wrapper.querySelectorAll(".row-fluid .special")[1].textContent.trim().replace(/(\r\n\t|\n|\r\t)/gm,"");
          guide.push({"query":query,"result":result});
      });

      GM_xmlhttpRequest({
          method: "POST",
          url: base_url+"/uksearch",
          data:JSON.stringify ( {"worker_id":worker_id, "name":guide} ),
          headers:    {
              Authorization: "Bearer " + jwtoken,
              "Content-Type": "application/json",
              "Accept": "application/json"
          },
          onload: function(response) {
              var json=JSON.parse(response.responseText)||0;
              if (!JSON.parse(response.responseText).errors && response.status==200) {
                  Object.entries(json).forEach(([key, value]) => {
                      task_names.push(value);
                  });
                  set(task_names);
              }else{
                  var errors = document.querySelectorAll(".jsawesome");
                  errors.forEach(error => {
                      var titleDiv = error.querySelector("div");
                          titleDiv.style.backgroundColor = "red";
                          titleDiv.scrollIntoView({ block: "center" });
                  });
              }
          },
          onerror:function(resp){
            console.log("Error");
            console.log(resp.status+' '+resp.statusText);
          },
          ontimeout:function(resp){
            console.log('Timeout');
          }
      });


      function set(task_names){

        const filterItems = query => {
              return Object.values(Object.values(task_names).filter((el) =>el.name.toLowerCase().indexOf(query.name.toLowerCase()) > -1 && el.name2.toLowerCase().indexOf(query.name2.toLowerCase()) > -1));
        }

        var jsawesome = document.querySelectorAll(".jsawesome");

            jsawesome.forEach(wrapper => {
                var titleDiv = wrapper.querySelector("div");
                var query = wrapper.querySelectorAll(".row-fluid .special")[0].textContent.trim().replace(/(\r\n\t|\n|\r\t)/gm,"");
                var result = wrapper.querySelectorAll(".row-fluid .special")[1].textContent.trim().replace(/(\r\n\t|\n|\r\t)/gm,"");
                var pares_content = wrapper.querySelectorAll(".radios.cml_field");
                var fill = filterItems({'name':query,'name2':result});

                if(fill[0]?.name.toLowerCase()==query.toLowerCase() && fill[0]?.name2.toLowerCase()==result.toLowerCase()){
                    console.log("existe");
                    titleDiv.style.backgroundColor = "Aqua";
                    titleDiv.scrollIntoView({ block: "center" });

                    for(var preg of pares_content.entries()) {
                        let radios = preg[1].querySelectorAll(".radios.cml_field input");
                        let indice=fill[0].respuesta.charAt(preg[0])-1;
                        if(indice!==-1){
                            radios[indice].checked=1;
                            radios[indice].click();
                        }
                    }
                }else{
                    console.log("no existe");
                    for(var pregg of pares_content.entries()) {
                        let radios = pregg[1].querySelectorAll(".radios.cml_field input");
                        let st="2";
                        let indice=st.charAt(pregg[0])-1;
                        if(indice!==-1){
                            radios[indice].checked=1;
                            radios[indice].click();
                        }
                    }

                }

            });
     }



    function time_submit(min, max){
            return Math.floor(Math.random() * (max - min + 1) + min);
    }

    function ActivarReloj(){
        //Obteniendo datos del tiempo
        var laHora = new Date();
        var horario = laHora.getHours();
        var minutero = laHora.getMinutes();
        var segundero = laHora.getSeconds();

        if(minutero<10){
            minutero = "0" + minutero;
        }
        if(segundero<10){
            segundero = "0" + segundero;
        }

        var tiempo = document.querySelectorAll("#countdown_timer .countdown_row").length;

        if(tiempo != 0){
            let submit=time_submit(time_submit_min,time_submit_max);
            //console.log("se enviara en "+submit);
            document.title = "("+submit+") OPEN";
            setTimeout(function () {
                document.title = "CERRANDO";
                //document.querySelector(".btn-cf-blue").click();
            }, submit);
        }else{
            document.title = "TIEMPO ESPERA: "+horario+":"+minutero+":"+segundero;
            //Actualizando la hora cada 1 segundo
            setTimeout(ActivarReloj,1000);
        }

    }
    ActivarReloj();

}
//uk_search-science

//Signature Annotation: Job Title Extraction
if(jobTitle.includes("Signature Annotation: Job Title Extraction")){

//tiempo en milisegundos !!!
var time_submit_min=3000;
var time_submit_max=3000;

        var jsawesome = document.querySelectorAll(".jsawesome");
        jsawesome.forEach(wrapper => {
                var pares_content = wrapper.querySelectorAll(".radios.cml_field");
                for(var preg of pares_content.entries()) {
                    let radios = preg[1].querySelectorAll(".radios.cml_field input");
                    let st="2";
                    let indice=st.charAt(preg[0])-1;
                     if(indice!==-1){
                     radios[indice].checked=1;
                     radios[indice].click();
                     }
                }
        });


    function time_submit(min, max){
            return Math.floor(Math.random() * (max - min + 1) + min);
    }

     function ActivarReloj(){
        //Obteniendo datos del tiempo
        var laHora = new Date();
        var horario = laHora.getHours();
        var minutero = laHora.getMinutes();
        var segundero = laHora.getSeconds();

        if(minutero<10){
            minutero = "0" + minutero;
        }
        if(segundero<10){
            segundero = "0" + segundero;
        }

        var tiempo = document.querySelectorAll("#countdown_timer .countdown_row").length;

        if(tiempo != 0){
            let submit=time_submit(time_submit_min,time_submit_max);
            //console.log("se enviara en "+submit);
            document.title = "("+submit+") OPEN";

            setTimeout(function () {
                    document.title = "CERRANDO";
                    document.querySelector(".btn-cf-blue").click();
            }, submit);

        }else{
            document.title = "TIEMPO ESPERA: "+horario+":"+minutero+":"+segundero;
            //Actualizando la hora cada 1 segundo
            setTimeout(ActivarReloj,1000);
        }

    }

    ActivarReloj();

 }
//Signature Annotation: Job Title Extraction



})();